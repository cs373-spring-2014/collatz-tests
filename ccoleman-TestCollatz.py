#!/usr/bin/env python3

# -------------------------------
# projects/collatz/TestCollatz.py
# Copyright (C) 2014
# Glenn P. Downing
# -------------------------------

"""
To test the program:
    % python TestCollatz.py >& TestCollatz.out
    % chmod ugo+x TestCollatz.py
    % TestCollatz.py >& TestCollatz.out
"""

# -------
# imports
# -------

import io
import unittest

from Collatz import collatz_read_1, collatz_read_2, collatz_read_3, collatz_eval, collatz_print, collatz_solve, eval_cycle_len

# -----------
# TestCollatz
# -----------

class TestCollatz (unittest.TestCase) :
    # ----
    # read
    # ----

    def test_read_1 (self) :
        r = io.StringIO("1 10\n")
        m = collatz_read_1(r)
        i, j = list(next(m))
        self.assertTrue(i ==  1)
        self.assertTrue(j == 10)

    def test_read_2 (self) :
        r = io.StringIO("1 10\n")
        m = collatz_read_2(r)
        i, j = list(next(m))
        self.assertTrue(i ==  1)
        self.assertTrue(j == 10)

    def test_read_3 (self) :
        r = io.StringIO("1 10\n")
        m = collatz_read_3(r)
        i, j = list(next(m))
        self.assertTrue(i ==  1)
        self.assertTrue(j == 10)

    # ----
    # eval
    # ----

    def test_eval_1 (self) :
        v = collatz_eval(1, 10)
        self.assertTrue(v == 20)

    def test_eval_2 (self) :
        v = collatz_eval(100, 200)
        self.assertTrue(v == 125)

    def test_eval_3 (self) :
        v = collatz_eval(201, 210)
        self.assertTrue(v == 89)

    def test_eval_4 (self) :
        v = collatz_eval(900, 1000)
        self.assertTrue(v == 174)

    def test_eval_cycle_len_1 (self) :
        c = eval_cycle_len(1)
        self.assertTrue(c == 1)

    def test_eval_cycle_len_2 (self) :
        c = eval_cycle_len(2)
        self.assertTrue(c == 2)

    def test_eval_cycle_len_3 (self) :
        c = eval_cycle_len(9)
        self.assertTrue(c == 20)

    # -----
    # print
    # -----

    def test_print_1 (self) :
        w = io.StringIO()
        collatz_print(w, 1, 10, 20)
        self.assertTrue(w.getvalue() == "1 10 20\n")

    def test_print_2 (self) :
        w = io.StringIO()
        collatz_print(w, 100000000, 100000000000, 0)
        self.assertTrue(w.getvalue() == "100000000 100000000000 0\n")

    def test_print_3 (self) :
        w = io.StringIO()
        collatz_print(w, 1.5, 3.095, "hi")
        self.assertTrue(w.getvalue() == "1.5 3.095 hi\n")

    # -----
    # solve
    # -----

    def test_solve_1 (self) :
        r = io.StringIO("1 10\n100 200\n201 210\n900 1000\n")
        w = io.StringIO()
        collatz_solve(r, w)
        self.assertTrue(w.getvalue() == "1 10 20\n100 200 125\n201 210 89\n900 1000 174\n")

    def test_solve_2 (self) :
        r = io.StringIO("1 1")
        w = io.StringIO()
        collatz_solve(r, w)
        self.assertTrue(w.getvalue() == "1 1 1\n")

    def test_solve_3 (self) :
        r = io.StringIO("2 1")
        w = io.StringIO()
        collatz_solve(r, w)
        self.assertTrue(w.getvalue() == "2 1 2\n")

# ----
# main
# ----

print("TestCollatz.py")
unittest.main()
print("Done.")
