#!/usr/bin/env python3

# -------------------------------
# projects/collatz/TestCollatz.py
# Copyright (C) 2014
# Glenn P. Downing
# -------------------------------

"""
To test the program:
    % python TestCollatz.py >& TestCollatz.out
    % chmod ugo+x TestCollatz.py
    % TestCollatz.py >& TestCollatz.out
"""

# -------
# imports
# -------

import io
import unittest
import sys

from Collatz import collatz_read, collatz_eval, collatz_print, collatz_solve, get_cache, collatz_calculate

# -----------
# TestCollatz
# -----------

class TestCollatz (unittest.TestCase) :
    # ----
    # read
    # ----

    def test_read_1 (self) :
        r = io.StringIO("1 10\n")
        a = [0, 0]
        b = collatz_read(r, a)
        i, j = a
        self.assertTrue(b == True)
        self.assertTrue(i ==  1)
        self.assertTrue(j == 10)

    def test_read_2 (self) :
        r = io.StringIO("1\n")
        a = [0,0]
        b = collatz_read(r, a)
        self.assertTrue(b == False)

    def test_read_3 (self) :
        r = io.StringIO("1 2 3\n")
        a = [0,0]
        b = collatz_read(r, a)
        self.assertTrue(b == False)

    # ----
    # eval
    # ----

    def test_eval_1 (self) :
        cache = get_cache()
        v = collatz_eval(1, 10, cache)
        self.assertTrue(v == 20)

    def test_eval_2 (self) :
        cache = get_cache()
        v = collatz_eval(100, 200, cache)
        self.assertTrue(v == 125)

    def test_eval_3 (self) :
        cache = get_cache()
        v = collatz_eval(201, 210, cache)
        self.assertTrue(v == 89)

    def test_eval_3 (self) :
        cache = get_cache()
        v = collatz_eval(210, 200, cache)
        self.assertTrue(v == 89)

    def test_eval_4 (self) :
        cache = get_cache()
        v = collatz_eval(900, 1000, cache)
        self.assertTrue(v == 174)


    def test_eval_5 (self) :
        cache = get_cache()
        v = collatz_eval(1, 2001, cache)
        self.assertTrue(v == 182)
        
    def test_eval_6 (self) :
        cache = get_cache()
        v = collatz_eval(1, 10001, cache)
        self.assertTrue(v == 262)

    def test_eval_7 (self) :
        cache = get_cache()
        v = collatz_eval(1, 10001, cache)
        self.assertTrue(v == 262)

    # -----
    # get_cache()
    # -----

    def test_get_cache_1(self) :
        cache = get_cache()
        self.assertTrue(len(cache) == 1000)
    
    def test_get_cache_2(self) :
        cache = get_cache()
        self.assertTrue(cache[0] == 179)
        self.assertTrue(cache[583] == 434)


    # ---------
    # calculate
    # ---------
    def test_calculate_1 (self) :
        v = collatz_calculate(20)
        self.assertTrue(v == 8)

    def test_calculate_2 (self) :
        v = collatz_calculate(58213)
        self.assertTrue(v == 74)

    def test_calculate_3 (self) :
        v = collatz_calculate(1000000)
        self.assertTrue(v == 153)

    # -----
    # print
    # -----

    def test_print (self) :
        w = io.StringIO()
        collatz_print(w, 1, 10, 20)
        self.assertTrue(w.getvalue() == "1 10 20\n")

    def test_print_2 (self) :
        w = io.StringIO()
        collatz_print(w, 500, 2, 10)
        self.assertTrue(w.getvalue() == "500 2 10\n")

    # -----
    # solve
    # -----

    def test_solve (self) :
        r = io.StringIO("1 10\n100 200\n201 210\n900 1000\n")
        w = io.StringIO()
        collatz_solve(r, w)
        self.assertTrue(w.getvalue() == "1 10 20\n100 200 125\n201 210 89\n900 1000 174\n")

    def test_solve_2 (self) :
        r = io.StringIO("1160 73354\n53023 74345\n75324 4675\n")
        w = io.StringIO()
        collatz_solve(r, w)
        self.assertTrue(w.getvalue() == "1160 73354 340\n53023 74345 335\n75324 4675 340\n")

# ----
# main
# ----

print("TestCollatz.py")
unittest.main()
print("Done.")
