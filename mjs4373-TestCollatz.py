#!/usr/bin/env python

# -----------------------
# Mitch Stephan (mjs4373)
# -----------------------

# -------------------------------
# projects/collatz/TestCollatz.py
# Copyright (C) 2014
# Glenn P. Downing
# -------------------------------

"""
To test the program:
    % python TestCollatz.py > TestCollatz.out
    % chmod ugo+x TestCollatz.py
    % TestCollatz.py > TestCollatz.out
"""

# -------
# imports
# -------

import StringIO
import unittest

from Collatz import collatz_read_3, collatz_eval, collatz_print, collatz_solve

# -----------
# TestCollatz
# -----------

class TestCollatz (unittest.TestCase) :
    # ----
    # read
    # ----

    def test_read (self) :
        r = StringIO.StringIO("1 10\n")
        p = collatz_read_3(r)
        (i, j) = p.next()
        self.assert_(i ==  1)
        self.assert_(j == 10)

    def test_read_reverse (self) :
        r = StringIO.StringIO("10 1\n")
        p = collatz_read_3(r)
        (i, j) = p.next()
        self.assert_(i == 10)
        self.assert_(j == 1)

    def test_read_same (self) :
        r = StringIO.StringIO("1 1\n")
        p = collatz_read_3(r)
        (i, j) = p.next()
        self.assert_(i == 1)
        self.assert_(j == 1)

    def test_read_large_ints (self) :
        r = StringIO.StringIO("500000 500002\n")
        p = collatz_read_3(r)
        (i, j) = p.next()
        self.assert_(i == 500000)
        self.assert_(j == 500002)

    def test_read_full_range (self) :
        r = StringIO.StringIO("1 1000000\n")
        p = collatz_read_3(r)
        (i, j) = p.next()
        self.assert_(i == 1)
        self.assert_(j == 1000000)

    # ----
    # eval
    # ----

    def test_eval_1 (self) :
        v = collatz_eval((1, 10))
        self.assert_(v == 20)

    def test_eval_2 (self) :
        v = collatz_eval((100, 200))
        self.assert_(v == 125)

    def test_eval_3 (self) :
        v = collatz_eval((201, 210))
        self.assert_(v == 89)

    def test_eval_4 (self) :
        v = collatz_eval((900, 1000))
        self.assert_(v == 174)

    def test_eval_same (self) :
        v = collatz_eval((1, 1))
        self.assert_(v == 1)

    def test_eval_large_ints (self) :
        v = collatz_eval((500000, 500002))
        self.assert_(v == 152)

    def test_eval_full_range (self) :
        v = collatz_eval((1, 1000000))
        self.assert_(v == 525)

    # -----
    # print
    # -----

    def test_print (self) :
        w = StringIO.StringIO()
        collatz_print(w, (1, 10), 20)
        self.assert_(w.getvalue() == "1 10 20\n")

    def test_print_same (self) :
        w = StringIO.StringIO()
        collatz_print(w, (1, 1), 1)
        self.assert_(w.getvalue() == "1 1 1\n")

    def test_print_large_ints (self) :
        w = StringIO.StringIO()
        collatz_print(w, (500000, 500002), 152)
        self.assert_(w.getvalue() == "500000 500002 152\n")

    def test_print_full_Range (self) :
        w = StringIO.StringIO()
        collatz_print(w, (1, 1000000), 525)
        self.assert_(w.getvalue() == "1 1000000 525\n")


    # -----
    # solve
    # -----

    def test_solve (self) :
        r = StringIO.StringIO("1 10\n100 200\n201 210\n900 1000\n")
        w = StringIO.StringIO()
        collatz_solve(r, w)
        self.assert_(w.getvalue() == "1 10 20\n100 200 125\n201 210 89\n900 1000 174\n")

    def test_solve_reverse (self) :
        r = StringIO.StringIO("10 1\n")
        w = StringIO.StringIO()
        collatz_solve(r, w)
        self.assert_(w.getvalue() == "10 1 20\n")

    def test_solve_same (self) :
        r = StringIO.StringIO("1 1\n")
        w = StringIO.StringIO()
        collatz_solve(r, w)
        self.assert_(w.getvalue() == "1 1 1\n")

    def test_solve_larege_ints (self) :
        r = StringIO.StringIO("500000 500002\n")
        w = StringIO.StringIO()
        collatz_solve(r, w)
        self.assert_(w.getvalue() == "500000 500002 152\n")

    def test_solve_full_range (self) :
        r = StringIO.StringIO("1 1000000\n")
        w = StringIO.StringIO()
        collatz_solve(r, w)
        self.assert_(w.getvalue() == "1 1000000 525\n")

# ----
# main
# ----

print "TestCollatz.py"
unittest.main()
print "Done."