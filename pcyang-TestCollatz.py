#!/usr/bin/env python3

# -------------------------------
# projects/collatz/TestCollatz.py
# Copyright (C) 2014
# Glenn P. Downing
# -------------------------------

"""
To test the program:
    % python pcyang-TestCollatz.py >& pcyang-TestCollatz.out
    % chmod ugo+x pcyang-TestCollatz.py
    % pcyang-TestCollatz.py >& pcyang-TestCollatz.out
"""

# -------
# imports
# -------

import StringIO
import unittest

from Collatz import collatz_read, collatz_eval, collatz_cycle_len, collatz_print, collatz_solve

# -----------
# TestCollatz
# -----------

class TestCollatz (unittest.TestCase) :
    # ----
    # read
    # ----

    def test_read_1 (self) :
        r = StringIO.StringIO("1 10\n")
        a = collatz_read(r)
        i, j = list(a)[0]
        self.assertTrue(i ==  1)
        self.assertTrue(j == 10)

    def test_read_2 (self) :
        r = StringIO.StringIO("128 8\n")
        a = collatz_read(r)
        i, j = list(a)[0]
        self.assertTrue(i ==  128)
        self.assertTrue(j == 8)

    # ----
    # eval
    # ----

    def test_eval_1 (self) :
        v = collatz_eval(1, 10)
        self.assertTrue(v == 20)

    def test_eval_2 (self) :
        v = collatz_eval(100, 200)
        self.assertTrue(v == 125)

    def test_eval_3 (self) :
        v = collatz_eval(201, 210)
        self.assertTrue(v == 89)

    def test_eval_4 (self) :
        v = collatz_eval(900, 1000)
        self.assertTrue(v == 174)

    def test_eval_5 (self) :
        v = collatz_eval(1, 1)
        self.assertTrue(v == 1)

    def test_eval_6 (self) :
        v = collatz_eval(5, 5)
        self.assertTrue(v == 6)

    def test_eval_7 (self) :
        v = collatz_eval(8, 14)
        self.assertTrue(v == 20)

    def test_eval_8 (self) :
        v = collatz_eval(20, 16)
        self.assertTrue(v == 21)

    def test_eval_9 (self) :
        v = collatz_eval(31, 27)
        self.assertTrue(v == 112)

    def test_eval_10 (self) :
        v = collatz_eval(32, 64)
        self.assertTrue(v == 113)

    # ----
    # cycle_len
    # ----

    def test_cycle_len_1 (self) :
        len = collatz_cycle_len (1)
        self.assertTrue(len == 1)

    def test_cycle_len_2 (self) :
        len = collatz_cycle_len (9)
        self.assertTrue(len == 20)

    def test_cycle_len_3 (self) :
        len = collatz_cycle_len (32)
        self.assertTrue(len == 6)

    def test_cycle_len_4 (self) :
        len = collatz_cycle_len (97)
        self.assertTrue(len == 119)

    # -----
    # print
    # -----

    def test_print (self) :
        w = StringIO.StringIO()
        collatz_print(w, 1, 10, 20)
        self.assertTrue(w.getvalue() == "1 10 20\n")

    # -----
    # solve
    # -----

    def test_solve_1 (self) :
        r = StringIO.StringIO("1 1\n")
        w = StringIO.StringIO()
        collatz_solve(r, w)
        self.assertTrue(w.getvalue() == "1 1 1\n")

    def test_solve_2 (self) :
        r = StringIO.StringIO("1 10\n100 200\n201 210\n900 1000\n")
        w = StringIO.StringIO()
        collatz_solve(r, w)
        self.assertTrue(w.getvalue() == "1 10 20\n100 200 125\n201 210 89\n900 1000 174\n")

    def test_solve_3 (self) :
        r = StringIO.StringIO("5 5\n8 14\n20 16\n 31 27\n32 64\n")
        w = StringIO.StringIO()
        collatz_solve(r, w)
        self.assertTrue(w.getvalue() == "5 5 6\n8 14 20\n20 16 21\n31 27 112\n32 64 113\n")

# ----
# main
# ----

print("TestCollatz.py")
unittest.main()
print("Done.")