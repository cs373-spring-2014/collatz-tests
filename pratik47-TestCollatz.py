#!/usr/bin/env python

# -------------------------------
# projects/collatz/TestCollatz.py
# Copyright (C) 2014
# Glenn P. Downing
# -------------------------------

"""
To test the program:
    % python TestCollatz.py > TestCollatz.out
    % chmod ugo+x TestCollatz.py
    % TestCollatz.py > TestCollatz.out
"""

# -------
# imports
# -------

import StringIO
import unittest

from Collatz import collatz_read, collatz_eval, collatz_print, collatz_solve, collatz_eval_length

# -----------
# TestCollatz
# -----------

class TestCollatz (unittest.TestCase) :
    # ----
    # read
    # ----

    def test_read_1 (self) :
        r = StringIO.StringIO("1 10\n")
        p = collatz_read(r)
        (i, j) = p.next()
        self.assert_(i ==  1)
        self.assert_(j == 10)

    def test_read_2 (self) :
        r = StringIO.StringIO("10 110\n")
        p = collatz_read(r)
        (i, j) = p.next()
        self.assert_(i ==  10)
        self.assert_(j == 110)

    def test_read_3 (self) :
        r = StringIO.StringIO("50 10\n")
        p = collatz_read(r)
        (i, j) = p.next()
        self.assert_(i ==  50)
        self.assert_(j == 10)

    # ----
    # read
    # ----

    def test_collatz_eval_length_1(self) :
        n = 15
        result = collatz_eval_length(n)
        self.assert_(result == 18)


    def test_collatz_eval_length_2(self) :
        n = 23
        result = collatz_eval_length(n)
        self.assert_(result == 16)

    def test_collatz_eval_length_3(self) :
        n = 5
        result = collatz_eval_length(n)
        self.assert_(result == 6)

    def test_collatz_eval_length_4(self) :
        n = 27
        result = collatz_eval_length(n)
        self.assert_(result == 112)

    # ----
    # eval
    # ----
    

    def test_eval_1 (self) :
        v = collatz_eval((1, 10))
        self.assert_(v == 20)

    def test_eval_2 (self) :
        v = collatz_eval((100, 200))
        self.assert_(v == 125)

    def test_eval_3 (self) :
        v = collatz_eval((201, 210))
        self.assert_(v == 89)

    def test_eval_4 (self) :
        v = collatz_eval((900, 1000))
        self.assert_(v == 174)

    # -----
    # print
    # -----

    def test_print_1 (self) :
        w = StringIO.StringIO()
        collatz_print(w, (1, 10), 20)
        self.assert_(w.getvalue() == "1 10 20\n")

    def test_print_2 (self) :
        w = StringIO.StringIO()
        collatz_print(w, (100, 200), 125)
        self.assert_(w.getvalue() == "100 200 125\n")

    def test_print_3 (self) :
        w = StringIO.StringIO()
        collatz_print(w, (201, 210), 89)
        self.assert_(w.getvalue() == "201 210 89\n")

    # -----
    # solve
    # -----

    def test_solve_1 (self) :
        r = StringIO.StringIO("1 10\n100 200\n201 210\n900 1000\n")
        w = StringIO.StringIO()
        collatz_solve(r, w)
        self.assert_(w.getvalue() == "1 10 20\n100 200 125\n201 210 89\n900 1000 174\n")

    def test_solve_2 (self) :
        r = StringIO.StringIO("362612 160835\n955140 498138\n873641 219618\n286502 813953")
        w = StringIO.StringIO()
        collatz_solve(r, w)
        self.assert_(w.getvalue() == "362612 160835 443\n955140 498138 525\n873641 219618 525\n286502 813953 509\n")

    def test_solve_3 (self) :
        r = StringIO.StringIO("563747 49790\n194684 595661\n76840 584441\n734650 447039\n29628 829703\n988674 409775\n")
        w = StringIO.StringIO()
        collatz_solve(r, w)
        self.assert_(w.getvalue() == "563747 49790 470\n194684 595661 470\n76840 584441 470\n734650 447039 509\n29628 829703 509\n988674 409775 525\n")

# ----
# main
# ----

print "TestCollatz.py"
unittest.main()
print "Done."
