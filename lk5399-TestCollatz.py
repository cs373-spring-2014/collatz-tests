#!/usr/bin/env python3

# -------------------------------
# projects/collatz/TestCollatz.py
# Copyright (C) 2014
# Glenn P. Downing
# -------------------------------

#!/usr/bin/env python3

# -------------------------------
# projects/collatz/TestCollatz.py
# Copyright (C) 2014
# Glenn P. Downing
# -------------------------------

"""
To test the program:
    % python TestCollatz.py > TestCollatz.out
    % chmod ugo+x TestCollatz.py
    % TestCollatz.py > TestCollatz.out
"""

# -------
# imports
# -------


import StringIO
import unittest
import sys


from Collatz import collatz_read, collatz_eval, collatz_print, collatz_solve, calcLength

# -----------
# TestCollatz
# -----------





class TestCollatz (unittest.TestCase) :
    # ----
    # read
    # ----

    def test_read (self) :
        r = StringIO.StringIO("1 10\n")
        a = [0, 0]
        b = collatz_read(r, a)
        i, j = a
        self.assertTrue(b == True)
        self.assertTrue(i ==  1)
        self.assertTrue(j == 10)

    def test_read2 (self) :
        r = StringIO.StringIO("2 16\n")
        a = [0, 0]
        b = collatz_read(r, a)
        i, j = a
        self.assertTrue(b == True)
        self.assertTrue(i ==  2)
        self.assertTrue(j == 16)

    def test_read3 (self) :
        r = StringIO.StringIO("9029 10000\n")
        a = [0, 0]
        b = collatz_read(r, a)
        i, j = a
        self.assertTrue(b == True)
        self.assertTrue(i ==  9029)
        self.assertTrue(j == 10000)

    def test_read4 (self) :
        r = StringIO.StringIO("456 10000\n")
        a = [0, 0]
        b = collatz_read(r, a)
        i, j = a
        self.assertTrue(b == True)
        self.assertTrue(i ==  456)
        self.assertTrue(j == 10000)


    #------
    # calcLength
    #------
    def test_CalcLength (self) :
        num=10
        count=0
        cache=dict()
        s = calcLength(num,count,cache)
        self.assertTrue(s == 7)

    def test_CalcLength2 (self) :
        num=5
        count=0
        cache=dict()
        s = calcLength(num,count,cache)
        self.assertTrue(s == 6)

    def test_CalcLength2 (self) :
        num=22
        count=0
        cache=dict()
        s = calcLength(num,count,cache)
        self.assertTrue(s == 16)






    # ----
    # eval
    # ----

    def test_eval_1 (self) :
        v = collatz_eval(1, 10)

        self.assertTrue(v == 20)

    def test_eval_2 (self) :
        v = collatz_eval(100, 200)


        self.assertTrue(v == 125)

    def test_eval_3 (self) :
        v = collatz_eval(201, 210)


        self.assertTrue(v == 89)

    def test_eval_4 (self) :
        v = collatz_eval(900, 1000)

        self.assertTrue(v == 174)

    def test_eval_5 (self) :
        v = collatz_eval(1000, 900)

        self.assertTrue(v == 174)

    def test_eval_6 (self) :
        v = collatz_eval(1, 1)

        self.assertTrue(v == 1)

    def test_eval_7 (self) :
        v = collatz_eval(1, 2)

        self.assertTrue(v == 2)

    # -----
    # print
    # -----

    def test_print (self) :
        w = StringIO.StringIO()
        collatz_print(w, 1, 10, 20)
        self.assertTrue(w.getvalue() == "1 10 20\n")

    def test_print2 (self) :
        w = StringIO.StringIO()
        collatz_print(w, 100, 200, 125)
        self.assertTrue(w.getvalue() == "100 200 125\n")

    def test_print3 (self) :
        w = StringIO.StringIO()
        collatz_print(w, 201, 210, 89)
        self.assertTrue(w.getvalue() == "201 210 89\n")

    def test_print4 (self) :
        w = StringIO.StringIO()
        collatz_print(w, 900, 1000, 174)
        self.assertTrue(w.getvalue() == "900 1000 174\n")

    # -----
    # solve
    # -----

    def test_solve (self) :
        r = StringIO.StringIO("1 10\n100 200\n201 210\n900 1000\n")
        w = StringIO.StringIO()
        collatz_solve(r, w)
        self.assertTrue(w.getvalue() == "1 10 20\n100 200 125\n201 210 89\n900 1000 174\n")

    def test_solve2 (self) :
        r = StringIO.StringIO("1 1\n2 2\n")
        w = StringIO.StringIO()
        collatz_solve(r, w)
        self.assertTrue(w.getvalue() == "1 1 1\n2 2 1\n")

    def test_solve3 (self) :
        r = StringIO.StringIO("201 210\n")
        w = StringIO.StringIO()
        collatz_solve(r, w)
        self.assertTrue(w.getvalue() == "201 210 89\n")
    def test_solve4 (self) :
        r = StringIO.StringIO("1 2\n")
        w = StringIO.StringIO()
        collatz_solve(r, w)
        self.assertTrue(w.getvalue() == "1 2 2\n")
# ----
# main
# ----

print("TestCollatz.py")
unittest.main()
print("Done.")
