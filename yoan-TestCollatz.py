#!/usr/bin/env python3

# -------------------------------
# projects/collatz/TestCollatz.py
# Copyright (C) 2014
# Glenn P. Downing
# -------------------------------

"""
To test the program:
    % python TestCollatz.py >& TestCollatz.out
    % chmod ugo+x TestCollatz.py
    % TestCollatz.py >& TestCollatz.out
"""

# -------
# imports
# -------

import io
import unittest

from Collatz import collatz_read, collatz_eval, collatz_print, collatz_solve, collatz_cycle

# -----------
# TestCollatz
# -----------

class TestCollatz (unittest.TestCase) :

    # -----
    # cycle
    # -----

    def test_cycle_1 (self) :
        self.assertTrue(1 == collatz_cycle(1))

    def test_cycle_2 (self) :
        self.assertTrue(6 == collatz_cycle(5))

    def test_cycle_3 (self) :
        self.assertTrue(5 == collatz_cycle(16))

    def test_cycle_4 (self) :
        self.assertTrue(17 == collatz_cycle(7))

    def test_cycle_5 (self) :
        self.assertTrue(16 == collatz_cycle(22))

    # ----
    # read
    # ----

    def test_read_1 (self) :
        r = io.StringIO("1 10\n")
        a = [0, 0]
        b = collatz_read(r, a)
        i, j = a
        self.assertTrue(b == True)
        self.assertTrue(i ==  1)
        self.assertTrue(j == 10)

    def test_read_2 (self) :
        r = io.StringIO("")
        a = [1, 2]
        b = collatz_read(r, a)
        i, j = a
        self.assertTrue(b == False)
        self.assertTrue(i == 1)
        self.assertTrue(j == 2)

    def test_read_3 (self) :
        r = io.StringIO(" 1344 1034 \n")
        a = [0, 0]
        b = collatz_read(r, a)
        i, j = a
        self.assertTrue(b == True)
        self.assertTrue(i == 1344)
        self.assertTrue(j == 1034)

    # ----
    # eval
    # ----

    def test_eval_1 (self) :
        v = collatz_eval(1, 10)
        self.assertTrue(v == 20)

    def test_eval_2 (self) :
        v = collatz_eval(100, 200)
        self.assertTrue(v == 125)

    def test_eval_3 (self) :
        v = collatz_eval(201, 210)
        self.assertTrue(v == 89)

    def test_eval_4 (self) :
        v = collatz_eval(900, 1000)
        self.assertTrue(v == 174)

    # -----
    # print
    # -----

    def test_print_1 (self) :
        w = io.StringIO()
        collatz_print(w, 1, 10, 20)
        self.assertTrue(w.getvalue() == "1 10 20\n")

    def test_print_2 (self) :
        w = io.StringIO()
        collatz_print(w, 1, 1, 20)
        self.assertTrue(w.getvalue() == "1 1 20\n")

    def test_print_3 (self) :
        w = io.StringIO()
        collatz_print(w, 1000, 10, 20)
        self.assertTrue(w.getvalue() == "1000 10 20\n")

    # -----
    # solve
    # -----

    def test_solve_1 (self) :
        r = io.StringIO("1 10\n100 200\n201 210\n900 1000\n")
        w = io.StringIO()
        collatz_solve(r, w)
        self.assertTrue(w.getvalue() == "1 10 20\n100 200 125\n201 210 89\n900 1000 174\n")

    def test_solve_2 (self) :
        r = io.StringIO("100 200")
        w = io.StringIO()
        collatz_solve(r, w)
        self.assertTrue(w.getvalue() == "100 200 125\n")

    def test_solve_3 (self) :
        r = io.StringIO("")
        w = io.StringIO()
        collatz_solve(r, w)
        self.assertTrue(w.getvalue() == "")

# ----
# main
# ----

print("TestCollatz.py")
unittest.main()
print("Done.")
