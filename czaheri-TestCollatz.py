#!/usr/bin/env python


# -------------------------------
# projects/collatz/TestCollatz.py
# Copyright (C) 2014
# Glenn P. Downing
# -------------------------------

"""
To test the program:
    % python TestCollatz.py >& TestCollatz.out
    % chmod ugo+x TestCollatz.py
    % TestCollatz.py >& TestCollatz.out
"""

# -------
# imports
# -------

import StringIO
import unittest

from Collatz import collatz_read, collatz_eval, collatz_print, collatz_solve

# -----------
# TestCollatz
# -----------

class TestCollatz (unittest.TestCase) :
    # ----
    # read
    # ----

    def test_read_1 (self) :
        r = StringIO.StringIO("11 10\n")
        m = collatz_read(r)
        i, j = list(next(m))
        self.assertTrue(i == 11)
        self.assertTrue(j == 10)

    def test_read_2 (self) :
        r = StringIO.StringIO("1 1\n")
        m = collatz_read(r)
        i, j = list(next(m))
        self.assertTrue(i == 1)
        self.assertTrue(j == 1)

    def test_read_3 (self) :
        r = StringIO.StringIO("10 30\n")
        m = collatz_read(r)
        i, j = list(next(m))
        self.assertTrue(i == 10)
        self.assertTrue(j == 30)

    # ----
    # eval
    # ----

    def test_eval_1 (self) :
        v = collatz_eval(1, 10)
        self.assertTrue(v == 20)

    def test_eval_2 (self) :
        v = collatz_eval(100, 200)
        self.assertTrue(v == 125)

    def test_eval_3 (self) :
        v = collatz_eval(201, 210)
        self.assertTrue(v == 89)

    def test_eval_4 (self) :
        v = collatz_eval(900, 1000)
        self.assertTrue(v == 174)

    # -----
    # print
    # -----

    def test_print_1 (self) :
        w = StringIO.StringIO()
        collatz_print(w, 1, 10, 20)
        self.assertTrue(w.getvalue() == "1 10 20\n")

    def test_print_2 (self) :
        w = StringIO.StringIO()
        collatz_print(w, 5125, 8514, 262)
        self.assertTrue(w.getvalue() == "5125 8514 262\n")

    def test_print_3 (self) :
        w = StringIO.StringIO()
        collatz_print(w, 4554, 893, 238)
        self.assertTrue(w.getvalue() == "4554 893 238\n")

    # -----
    # solve
    # -----

    def test_solve_1 (self) :
        r = StringIO.StringIO("1 10\n100 200\n201 210\n900 1000\n")
        w = StringIO.StringIO()
        collatz_solve(r, w)
        self.assertTrue(w.getvalue() == "1 10 20\n100 200 125\n201 210 89\n900 1000 174\n")

    def test_solve_2 (self) :
        r = StringIO.StringIO("8514 5125\n")
        w = StringIO.StringIO()
        collatz_solve(r, w)
        self.assertTrue(w.getvalue() == "8514 5125 262\n")

    def test_solve_3 (self) :
        r = StringIO.StringIO("1 1\n")
        w = StringIO.StringIO()
        collatz_solve(r, w)
        self.assertTrue(w.getvalue() == "1 1 1\n")

# ----
# main
# ----

print("TestCollatz.py")
unittest.main()
print("Done.")
