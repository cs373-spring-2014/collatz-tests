#!/usr/bin/env python

# -------------------------------
# projects/collatz/TestCollatz.py
# Copyright (C) 2014
# Glenn P. Downing
# -------------------------------

"""
To test the program:
    % python TestCollatz.py > TestCollatz.out
    % chmod ugo+x TestCollatz.py
    % TestCollatz.py > TestCollatz.out
"""

# -------
# imports
# -------

import StringIO
import unittest

from Collatz import collatz_read, collatz_eval, collatz_print, collatz_solve

# -----------
# TestCollatz
# -----------

class TestCollatz (unittest.TestCase) :
    # ----
    # read
    # ----

    def test_read_1 (self) :
        r = StringIO.StringIO("1 10\n")
        a = [0, 0]
        b = collatz_read(r, a)
        self.assert_(b    == True)
        self.assert_(a[0] ==  1)
        self.assert_(a[1] == 10)

    def test_read_2 (self) :
        r = StringIO.StringIO("20 25\n")
        a = [0, 0]
        b = collatz_read(r, a)
        self.assert_(b == True)
        self.assert_(a[0] ==  20)
        self.assert_(a[1] == 25)

    def test_read_3 (self) :
        r = StringIO.StringIO("52 2\n")
        a = [0, 0]
        b = collatz_read(r, a)
        self.assert_(b    == True)
        self.assert_(a[0] == 52)
        self.assert_(a[1] == 2)


    def test_read_4 (self) :
        r = StringIO.StringIO("1999 2454545\n")
        a = [0, 0]
        b = collatz_read(r, a)
        self.assert_(b    == True)
        self.assert_(a[0] == 1999)
        self.assert_(a[1] == 2454545)

    # ----
    # eval
    # ----

    def test_eval_1 (self) :
        v = collatz_eval(1, 10)
        self.assert_(v == 20)

    def test_eval_2 (self) :
        v = collatz_eval(100, 200)
        self.assert_(v == 125)

    def test_eval_3 (self) :
        v = collatz_eval(201, 210)
        self.assert_(v == 89)

    def test_eval_4 (self) :
        v = collatz_eval(900, 1000)
        self.assert_(v == 174)

    def test_eval_5 (self) :
        v = collatz_eval(987, 863)
        self.assert_(v == 179)

    def test_eval_6 (self) :
        v = collatz_eval(233, 203)
        self.assert_(v == 128)
    def test_eval_7 (self) :
        v = collatz_eval(949, 968)
        self.assert_(v == 143)

    # -----
    # print
    # -----

    def test_print (self) :
        w = StringIO.StringIO()
        collatz_print(w, 1, 10, 20)
        self.assert_(w.getvalue() == "1 10 20\n")
    def test_print_1 (self) :
        w = StringIO.StringIO()
        collatz_print(w, 100, 200, 125)
        self.assert_(w.getvalue() == "100 200 125\n")
    def test_print_2 (self) :
        w = StringIO.StringIO()
        collatz_print(w, 201, 210, 89)
        self.assert_(w.getvalue() == "201 210 89\n")
    def test_print_3 (self) :
        w = StringIO.StringIO()
        collatz_print(w, 987, 863, 179)
        self.assert_(w.getvalue() == "987 863 179\n")
    def test_print_4 (self) :
        w = StringIO.StringIO()
        collatz_print(w, 233, 203, 128)
        self.assert_(w.getvalue() == "233 203 128\n")
    def test_print_5 (self) :
        w = StringIO.StringIO()
        collatz_print(w, 1, 999999, 525)
        self.assert_(w.getvalue() == "1 999999 525\n")



    # -----
    # solve
    # -----


    def test_solve_1 (self) :
        r = StringIO.StringIO("1 10\n100 200\n201 210\n900 1000\n")
        w = StringIO.StringIO()
        collatz_solve(r, w)
        self.assert_(w.getvalue() == "1 10 20\n100 200 125\n201 210 89\n900 1000 174\n")
    def test_solve_2 (self) :
        r = StringIO.StringIO("3 70\n5 248\n201 210\n4 232\n")
        w = StringIO.StringIO()
        collatz_solve(r, w)
        self.assert_(w.getvalue() == "3 70 113\n5 248 128\n201 210 89\n4 232 128\n")

    def test_solve_3 (self) :
        r = StringIO.StringIO("584 174\n815 339\n566 25")
        w = StringIO.StringIO()
        collatz_solve(r, w)
        self.assert_(w.getvalue() == "584 174 144\n815 339 171\n566 25 144\n")
    def test_solve_4 (self) :
        r = StringIO.StringIO("153 37\n498 106\n584 97")
        w = StringIO.StringIO()
        collatz_solve(r, w)
        self.assert_(w.getvalue() == "153 37 122\n498 106 144\n584 97 144\n")
    # ----
    # main
    # ----

print "TestCollatz.py"
unittest.main()
print "Done."
