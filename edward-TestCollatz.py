#!/usr/bin/env python3

# -------------------------------
# projects/collatz/TestCollatz.py
# Copyright (C) 2014
# Glenn P. Downing
# -------------------------------

"""
To test the program:
    % python TestCollatz.py >& TestCollatz.out
    % chmod ugo+x TestCollatz.py
    % TestCollatz.py >& TestCollatz.out
"""

# -------
# imports
# -------

import io
import unittest

from Collatz import collatz_read, collatz_eval, collatz_print, collatz_solve, collatz_cycle_length

# -----------
# TestCollatz
# -----------

class TestCollatz (unittest.TestCase) :
    # ----
    # read
    # ----

    def test_read (self) :
        r = io.StringIO("1 10\n")
        a = [0, 0]
        b = collatz_read(r, a)
        i, j = a
        self.assertTrue(b == True)
        self.assertTrue(i == 1)
        self.assertTrue(j == 10)

    # 3 more tests for read

    def test_read_2 (self) :
        r = io.StringIO("2 89\n")
        a = [0, 0]
        b = collatz_read(r, a)
        i, j = a
        self.assertTrue(b == True)
        self.assertTrue(i == 2)
        self.assertTrue(j == 89)    

    def test_read_3 (self) :
        r = io.StringIO("5 20\n")
        a = [0, 0]
        b = collatz_read(r, a)
        i, j = a
        self.assertTrue(b == True)
        self.assertTrue(i == 5)
        self.assertTrue(j == 20)  

    def test_read_4 (self) :
        r = io.StringIO("27 26\n")
        a = [0, 0]
        b = collatz_read(r, a)
        i, j = a
        self.assertTrue(b == True)
        self.assertTrue(i == 27)
        self.assertTrue(j == 26)      

    # ----
    # eval
    # ----

    def test_eval_1 (self) :
        v = collatz_eval(1, 10)
        self.assertTrue(v == 20)

    def test_eval_2 (self) :
        v = collatz_eval(100, 200)
        self.assertTrue(v == 125)

    def test_eval_3 (self) :
        v = collatz_eval(201, 210)
        self.assertTrue(v == 89)

    def test_eval_4 (self) :
        v = collatz_eval(900, 1000)
        self.assertTrue(v == 174)

    # 3 more unit tests for eval

    def test_eval_5 (self) :
        v = collatz_eval(97, 17)
        self.assertTrue(v == 119)

    def test_eval_6 (self) :
        v = collatz_eval(958, 630)
        self.assertTrue(v == 179)

    def test_eval_7 (self) :
        v = collatz_eval(7720, 4640)
        self.assertTrue(v == 262)

    # -----
    # print 
    # -----

    def test_print (self) :
        w = io.StringIO()
        collatz_print(w, 1, 10, 20)
        self.assertTrue(w.getvalue() == "1 10 20\n")

    # 3 more unit tests for print    

    def test_print_2 (self) : 
        w = io.StringIO()
        collatz_print(w, 7720, 4640, 262)
        self.assertTrue(w.getvalue() == "7720 4640 262\n")

    def test_print_3 (self) : 
        w = io.StringIO()
        collatz_print(w, 958, 630, 179)
        self.assertTrue(w.getvalue() == "958 630 179\n")

    def test_print_4 (self) : 
        w = io.StringIO()
        collatz_print(w, 97, 17, 119)
        self.assertTrue(w.getvalue() == "97 17 119\n")


    # -----
    # solve
    # -----

    def test_solve (self) :
        r = io.StringIO("1 10\n100 200\n201 210\n900 1000\n")
        w = io.StringIO()
        collatz_solve(r, w)
        self.assertTrue(w.getvalue() == "1 10 20\n100 200 125\n201 210 89\n900 1000 174\n")

    # 3 more unit tests for solve
    def test_solve_2 (self) :
        r = io.StringIO("7720 4640\n200 100\n958 630\n17 97\n")
        w = io.StringIO()
        collatz_solve(r, w)
        self.assertTrue(w.getvalue() == "7720 4640 262\n200 100 125\n958 630 179\n17 97 119\n")

    def test_solve_3 (self) :
        r = io.StringIO("10 1\n200 100\n210 201\n1000 900\n")
        w = io.StringIO()
        collatz_solve(r, w)
        self.assertTrue(w.getvalue() == "10 1 20\n200 100 125\n210 201 89\n1000 900 174\n")

    def test_solve_4 (self) :
        r = io.StringIO("468 74334\n 552 9048\n2947 658\n19575 3800\n")
        w = io.StringIO()
        collatz_solve(r, w)
        self.assertTrue(w.getvalue() == "468 74334 340\n552 9048 262\n2947 658 217\n19575 3800 279\n")


    # -----
    # cycle length
    # -----
    def test_cycle_length_1 (self) : 
        self.assertTrue(collatz_cycle_length(1) == 1)

    def test_cycle_length_2 (self) : 
        self.assertTrue(collatz_cycle_length(5) == 6)

    def test_cycle_length_3 (self) : 
        self.assertTrue(collatz_cycle_length(10) == 7)

# ----
# main
# ----

print("TestCollatz.py")
unittest.main()
print("Done.")
