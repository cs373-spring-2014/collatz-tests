#!/usr/bin/env python3

# -------------------------------
# projects/collatz/TestCollatz.py
# Copyright (C) 2014
# Glenn P. Downing
# -------------------------------

"""
To test the program:
    % python TestCollatz.py >& TestCollatz.out
    % chmod ugo+x TestCollatz.py
    % TestCollatz.py >& TestCollatz.out
"""

# -------
# imports
# -------

import StringIO
import unittest

from Collatz import collatz_read, collatz_eval, cycle_length, collatz_print, collatz_solve

# -----------
# TestCollatz
# -----------

class TestCollatz (unittest.TestCase) :
    # ----
    # read
    # ----

    def test_read_1 (self) :
        r = StringIO.StringIO("1 10\n")
        m = collatz_read(r)
        i, j = list(next(m))
        self.assertTrue(i ==  1)
        self.assertTrue(j == 10)

    def test_read_2 (self) :
        r = StringIO.StringIO("10 1\n")
        m = collatz_read(r)
        i, j = list(next(m))
        self.assertTrue(i ==  10)
        self.assertTrue(j == 1)

    def test_read_3 (self) :
        r = StringIO.StringIO("50000 10000\n")
        m = collatz_read(r)
        i, j = list(next(m))
        self.assertTrue(i ==  50000)
        self.assertTrue(j == 10000)

    # ----
    # eval
    # ----

    def test_eval_1 (self) :
        v = collatz_eval(1, 10)
        self.assertTrue(v == 20)

    def test_eval_2 (self) :
        v = collatz_eval(100, 200)
        self.assertTrue(v == 125)

    def test_eval_3 (self) :
        v = collatz_eval(201, 210)
        self.assertTrue(v == 89)

    def test_eval_4 (self) :
        v = collatz_eval(900, 1000)
        self.assertTrue(v == 174)

    # ------------
    # cycle_length
    # ------------

    def test_cycle_length_1 (self) :
        v = cycle_length(1)
        self.assertTrue(v == 1)

    def test_cycle_length_2 (self) :
        v = cycle_length(5)
        self.assertTrue(v == 6)

    def test_cycle_length_3 (self) :
        v = cycle_length(10)
        self.assertTrue(v == 7)

    def test_cycle_length_4 (self) :
        v = cycle_length(13)
        self.assertTrue(v == 10)
    # -----
    # print
    # -----

    def test_print_1 (self) :
        w = StringIO.StringIO()
        collatz_print(w, 1, 10, 20)
        self.assertTrue(w.getvalue() == "1 10 20\n")

    def test_print_2 (self) :
        w = StringIO.StringIO()
        collatz_print(w, 100, 200, 125)
        self.assertTrue(w.getvalue() == "100 200 125\n")

    def test_print_3 (self) :
        w = StringIO.StringIO()
        collatz_print(w, 770, 942, 179)
        self.assertTrue(w.getvalue() == "770 942 179\n")

    # -----
    # solve
    # -----

    def test_solve_1 (self) :
        r = StringIO.StringIO("1 10\n100 200\n201 210\n900 1000\n")
        w = StringIO.StringIO()
        collatz_solve(r, w)
        self.assertTrue(w.getvalue() == "1 10 20\n100 200 125\n201 210 89\n900 1000 174\n")

    def test_solve_2 (self) :
        r = StringIO.StringIO("987 279\n770 942\n")
        w = StringIO.StringIO()
        collatz_solve(r, w)
        self.assertTrue(w.getvalue() == "987 279 179\n770 942 179\n")

    def test_solve_3 (self) :
        r = StringIO.StringIO("402 55\n539 51\n")
        w = StringIO.StringIO()
        collatz_solve(r, w)
        self.assertTrue(w.getvalue() == "402 55 144\n539 51 144\n")

# ----
# main
# ----

print("TestCollatz.py")
unittest.main()
print("Done.")
