#!/usr/bin/env python3

# -------------------------------
# projects/collatz/TestCollatz.py
# Copyright (C) 2014
# Glenn P. Downing
# -------------------------------

"""
To test the program:
    % python TestCollatz.py >& TestCollatz.out
    % chmod ugo+x TestCollatz.py
    % TestCollatz.py >& TestCollatz.out
"""

# -------
# imports
# -------

import io
import unittest

from Collatz import collatz_read, collatz_eval, collatz_single_eval, collatz_print, collatz_solve

# -----------
# TestCollatz
# -----------

class TestCollatz (unittest.TestCase) :
    # ----
    # read
    # ----

    def test_read_1 (self) :
        r = io.StringIO("1 10\n")
        a = [0, 0]
        b = collatz_read(r, a)
        i, j = a
        self.assertTrue(b == True)
        self.assertTrue(i ==  1)
        self.assertTrue(j == 10)
        
    def test_read_2 (self) :
        r = io.StringIO("10 1\n")
        a = [0, 0]
        b = collatz_read(r, a)
        i, j = a
        self.assertTrue(b == True)
        self.assertTrue(i == 10)
        self.assertTrue(j == 1)
        
    def test_read_3 (self) :
        r = io.StringIO("\n")
        a = []
        b = collatz_read(r, a)
        self.assertTrue(b == False)
        
    def test_read_4 (self) :
        r = io.StringIO("10\n")
        a = [0]
        b = collatz_read(r, a)
        i = a
        self.assertTrue(b == False)
        self.assertTrue(i == [10])
        
    def test_read_5 (self) :
        r = io.StringIO("674576 674576\n")
        a = [0, 0]
        b = collatz_read(r, a)
        i, j = a
        self.assertTrue(b == True)
        self.assertTrue(i == 674576)
        self.assertTrue(j == 674576)

    # ----
    # eval
    # ----

    def test_eval_1 (self) :
        v = collatz_eval(1, 10)
        self.assertTrue(v == 20)

    def test_eval_2 (self) :
        v = collatz_eval(100, 200)
        self.assertTrue(v == 125)

    def test_eval_3 (self) :
        v = collatz_eval(201, 210)
        self.assertTrue(v == 89)

    def test_eval_4 (self) :
        v = collatz_eval(900, 1000)
        self.assertTrue(v == 174)
        
    def test_eval_5 (self) :
        v = collatz_eval(1, 999999)
        self.assertTrue(v == 525)
        
    def test_eval_6 (self) :
        v = collatz_eval(1000, 900)
        self.assertTrue(v == 174)
        
    # -----------
    # single_eval
    # -----------
    
    def test_single_eval_1(self) :
        v = collatz_single_eval(1000)
        self.assertTrue(v == 112)
        
    def test_single_eval_2(self) :
        v = collatz_single_eval(1)
        self.assertTrue(v == 1)
        
    def test_single_eval_3(self) :
        v = collatz_single_eval(999999)
        self.assertTrue(v == 259)

    # -----
    # print
    # -----

    def test_print_1 (self) :
        w = io.StringIO()
        collatz_print(w, 1, 10, 20)
        self.assertTrue(w.getvalue() == "1 10 20\n")
        
    def test_print_2 (self) :
        w = io.StringIO()
        collatz_print(w, 10, 1, 20)
        self.assertTrue(w.getvalue() == "10 1 20\n")

    # -----
    # solve
    # -----

    def test_solve_1 (self) :
        r = io.StringIO("1 10\n100 200\n201 210\n900 1000\n")
        w = io.StringIO()
        collatz_solve(r, w)
        self.assertTrue(w.getvalue() == "1 10 20\n100 200 125\n201 210 89\n900 1000 174\n")
        
    def test_solve_2 (self) :
        r = io.StringIO("1 1\n5 5\n1 10\n10 1")
        w = io.StringIO()
        collatz_solve(r, w)
        self.assertTrue(w.getvalue() == "1 1 1\n5 5 6\n1 10 20\n10 1 20\n")

# ----
# main
# ----

print("TestCollatz.py")
unittest.main()
print("Done.")

