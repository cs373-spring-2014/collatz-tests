#!/usr/bin/env python3

# -------------------------------
# projects/collatz/TestCollatz.py
# Copyright (C) 2014
# Glenn P. Downing
# -------------------------------

"""
To test the program:
    % python TestCollatz.py > TestCollatz.out
    % chmod ugo+x TestCollatz.py
    % TestCollatz.py > TestCollatz.out
"""

# -------
# imports
# -------

import io
import unittest

from Collatz import collatz_read, collatz_eval, collatz_print, collatz_solve, naive_solve, simple_cache_solve, memoized_solve

# -----------
# TestCollatz
# -----------

class TestCollatz (unittest.TestCase) :
    # ----
    # read
    # ----

    def test_read_1 (self) :
        r = io.StringIO("1 10\n")
        a = [0, 0]
        b = collatz_read(r, a)
        i, j = a
        self.assertTrue(b == True)
        self.assertTrue(i ==  1)
        self.assertTrue(j == 10)

    def test_read_2 (self) :
        r = io.StringIO("2 20\n")
        a = [0, 0]
        b = collatz_read(r, a)
        i, j = a
        self.assertTrue(b == True)
        self.assertTrue(i ==  2)
        self.assertTrue(j == 20)

    def test_read_3 (self) :
        r = io.StringIO("3 30\n")
        a = [0, 0]
        b = collatz_read(r, a)
        i, j = a
        self.assertTrue(b == True)
        self.assertTrue(i ==  3)
        self.assertTrue(j == 30)
    # ----
    # eval
    # ----

    def test_eval_1 (self) :
        v = collatz_eval(1, 10)
        self.assertTrue(v == 20)

    def test_eval_2 (self) :
        v = collatz_eval(100, 200)
        self.assertTrue(v == 125)

    def test_eval_3 (self) :
        v = collatz_eval(201, 210)
        self.assertTrue(v == 89)

    def test_eval_4 (self) :
        v = collatz_eval(900, 1000)
        self.assertTrue(v == 174)

    # -----
    # print
    # -----

    def test_print_1 (self) :
        w = io.StringIO()
        collatz_print(w, 1, 10, 20)
        self.assertTrue(w.getvalue() == "1 10 20\n")

    def test_print_2 (self) :
        w = io.StringIO()
        collatz_print(w, 1, 2, 3)
        self.assertTrue(w.getvalue() == "1 2 3\n")

    def test_print_3 (self) :
        w = io.StringIO()
        collatz_print(w, 100, 2, 300)
        self.assertTrue(w.getvalue() == "100 2 300\n")

    def test_print_4 (self) :
        w = io.StringIO()
        collatz_print(w, 500, 600, 999)
        self.assertTrue(w.getvalue() == "500 600 999\n")
    # -----
    # solve
    # -----

    def test_solve_1 (self) :
        r = io.StringIO("1 10\n100 200\n201 210\n900 1000\n")
        w = io.StringIO()
        collatz_solve(r, w)
        self.assertTrue(w.getvalue() == "1 10 20\n100 200 125\n201 210 89\n900 1000 174\n")

    def test_solve_2 (self) :
        r = io.StringIO("1 100\n100 150\n200 250\n900 950\n")
        w = io.StringIO()
        collatz_solve(r, w)
        self.assertTrue(w.getvalue() == "1 100 119\n100 150 122\n200 250 128\n900 950 174\n")

    def test_solve_3 (self) :
        r = io.StringIO("157 200\n 9090 1414\n 2345 5444\n")
        w = io.StringIO()
        collatz_solve(r, w)
        self.assertTrue(w.getvalue() == "157 200 125\n9090 1414 262\n2345 5444 238\n")

    def test_solve_4 (self) :
        r = io.StringIO("1 1\n1000 1000\n1000000 1000000\n")
        w = io.StringIO()
        collatz_solve(r, w)
        self.assertTrue(w.getvalue() == "1 1 1\n1000 1000 112\n1000000 1000000 153\n")

    # -----------
    # naive_solve
    # -----------

    def test_naive_solve_1 (self) :
        a = naive_solve(1)
        self.assertTrue(a == 1)

    def test_naive_solve_2 (self) :
        a = naive_solve(100)
        self.assertTrue(a == 26)

    def test_naive_solve_3 (self) :
        a = naive_solve(250)
        self.assertTrue(a == 110)

    def test_naive_solve_4 (self) :
        a = naive_solve(1000)
        self.assertTrue(a == 112)

    def test_naive_solve_5 (self) :
        a = naive_solve(15151)
        self.assertTrue(a == 85)

    def test_naive_solve_6 (self) :
        a = naive_solve(9091)
        self.assertTrue(a == 141)

    # -----------
    # simple_cache__solve
    # -----------

    def test_simple_cache_solve_1 (self) :
        a = simple_cache_solve(1)
        self.assertTrue(a == 1)

    def test_simple_cache_solve_2 (self) :
        a = simple_cache_solve(100)
        self.assertTrue(a == 26)

    def test_simple_cache_solve_3 (self) :
        a = simple_cache_solve(250)
        self.assertTrue(a == 110)

    def test_simple_cache_solve_4 (self) :
        a = simple_cache_solve(1000)
        self.assertTrue(a == 112)

    def test_simple_cache_solve_5 (self) :
        a = simple_cache_solve(15151)
        self.assertTrue(a == 85)

    def test_simple_cache_solve_6 (self) :
        a = simple_cache_solve(9091)
        self.assertTrue(a == 141)

    # -----------
    # memoized_solve
    # -----------

    def test_memoized_solve_1 (self) :
        a = memoized_solve(1)
        self.assertTrue(a == 1)

    def test_memoized_solve_2 (self) :
        a = memoized_solve(100)
        self.assertTrue(a == 26)

    def test_memoized_solve_3 (self) :
        a = memoized_solve(250)
        self.assertTrue(a == 110)

    def test_memoized_solve_4 (self) :
        a = memoized_solve(1000)
        self.assertTrue(a == 112)

    def test_memoized_solve_5 (self) :
        a = memoized_solve(15151)
        self.assertTrue(a == 85)

    def test_memoized_solve_6 (self) :
        a = memoized_solve(9091)
        self.assertTrue(a == 141)
# ----
# main
# ----

print("TestCollatz.py")
unittest.main()
print("Done.")
