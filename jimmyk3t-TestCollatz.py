#!/usr/bin/env python3

# ----------------------------------
# projects/collatz/TestCollatz.py
# Copyright (C) 2014
# Glenn P. Downing and James Kettler
# ----------------------------------

"""
To test the program:
    % python TestCollatz.py >& TestCollatz.out
    % chmod ugo+x TestCollatz.py
    % TestCollatz.py >& TestCollatz.out
"""

# -------
# imports
# -------

import io
import unittest

from Collatz import collatz_read, collatz_eval, collatz_print, collatz_solve, even_case, odd_case, collatz_unit, \
    collatz_generator

# -----------
# TestCollatz
# -----------

class TestCollatz(unittest.TestCase):
    # ----
    # read
    # ----

    def test_read(self):
        r = io.StringIO("1 10\n")
        a = [0, 0]
        b = collatz_read(r, a)
        i, j = a
        self.assertTrue(b == True)
        self.assertTrue(i == 1)
        self.assertTrue(j == 10)

    # ----
    # eval
    # ----

    def test_eval_1(self):
        v = collatz_eval(1, 10)
        self.assertTrue(v == 20)

    def test_eval_2(self):
        v = collatz_eval(100, 200)
        self.assertTrue(v == 125)

    def test_eval_3(self):
        v = collatz_eval(201, 210)
        self.assertTrue(v == 89)

    def test_eval_4(self):
        v = collatz_eval(900, 1000)
        self.assertTrue(v == 174)

    def test_eval_5(self):
        v = collatz_eval(1000, 900)
        self.assertTrue(v == 174)

    def test_eval_6(self):
        v = collatz_eval(1, 9)
        self.assertTrue(v == 20)

    def test_eval_7(self):
        v = collatz_eval(10, 100)
        g = collatz_eval(50, 100)
        self.assertTrue(v == g)

    def test_eval_8(self):
        v = collatz_eval(6074, 8424)
        self.assertTrue(v == 262)

    # ---------
    # even_case
    # ---------

    def test_even_case_1(self):
        v = even_case(4)
        self.assertTrue(v == 2)

    def test_even_case_2(self):
        v = even_case(4)
        self.assertTrue(v % 2 == 0)

    # --------
    # odd_case
    # --------

    def test_odd_case_1(self):
        v = odd_case(5)
        self.assertTrue(v == 16)

    def test_odd_case_2(self):
        v = odd_case(5)
        self.assertTrue(v % 2 == 0)

    # -------------------------------
    # single 3n+1 cycle counter tests
    # -------------------------------

    def test_collatz_unit_1(self):
        c = collatz_unit(1)
        self.assertTrue(c == 1)

    def test_collatz_unit_2(self):
        c = collatz_unit(22)
        self.assertTrue(c == 16)

    def test_collatz_unit_3(self):
        c = collatz_unit(3)
        self.assertTrue(c == 8)

    def test_collatz_unit_4(self):
        c = collatz_unit(9)
        self.assertTrue(c == 20)

    def test_collatz_unit_5(self):
        c = collatz_unit(110)
        self.assertTrue(c == 114)

    def test_collatz_unit_6(self):
        c = collatz_unit(27)
        self.assertTrue(c == 112)

    # ------------------------------------------------
    # verifying the simpler solver works without cache
    # ------------------------------------------------

    def test_collatz_gen_1(self):
        c = collatz_generator(27)
        self.assertTrue(c == collatz_unit(27))

    def test_collatz_gen_2(self):
        c = collatz_generator(124)
        self.assertTrue(c == collatz_unit(124))

    def test_collatz_gen_3(self):
        c = collatz_generator(4424)
        self.assertTrue(c == collatz_unit(4424))

    def test_collatz_gen_4(self):
        c = collatz_generator(1256)
        self.assertTrue(c == collatz_unit(1256))

    # -----
    # print
    # -----

    def test_print(self):
        w = io.StringIO()
        collatz_print(w, 1, 10, 20)
        self.assertTrue(w.getvalue() == "1 10 20\n")

    # -----
    # solve
    # -----

    def test_solve(self):
        r = io.StringIO("1 10\n100 200\n201 210\n900 1000\n")
        w = io.StringIO()
        collatz_solve(r, w)
        self.assertTrue(w.getvalue() == "1 10 20\n100 200 125\n201 210 89\n900 1000 174\n")

# ----
# main
# ----

print("TestCollatz.py")
unittest.main()
print("Done.")
