#!/usr/bin/env python3

# -------------------------------
# projects/collatz/TestCollatz.py
# Copyright (C) 2014
# Glenn P. Downing
# -------------------------------

"""
To test the program:
    % python TestCollatz.py >& TestCollatz.out
    % chmod ugo+x TestCollatz.py
    % TestCollatz.py >& TestCollatz.out
"""

# -------
# imports
# -------

import io
import unittest

from Collatz import collatz_read, collatz_eval, collatz_print, collatz_solve

# -----------
# TestCollatz
# -----------

class TestCollatz (unittest.TestCase) :
    # ----
    # read
    # ----

    def test_read1 (self) :
        r = io.StringIO("1 10\n")
        a = [0, 0]
        b = collatz_read(r, a)
        i, j = a
        self.assertTrue(b == True)
        self.assertTrue(i ==  1)
        self.assertTrue(j == 10)
    def test_read2 (self) :
        r = io.StringIO("201 210\n")
        a = [0, 0]
        b = collatz_read(r, a)
        i, j = a
        self.assertTrue(b == True)
        self.assertTrue(i ==  201)
        self.assertTrue(j == 210)
    def test_read3 (self) :
        r = io.StringIO("100 300\n")
        a = [0, 0]
        b = collatz_read(r, a)
        i, j = a
        self.assertTrue(b == True)
        self.assertTrue(i ==  100)
        self.assertTrue(j == 300)
    def test_read4 (self) :
        r = io.StringIO("5 40\n")
        a = [0, 0]
        b = collatz_read(r, a)
        i, j = a
        self.assertTrue(b == True)
        self.assertTrue(i ==  5)
        self.assertTrue(j == 40)

    # ----
    # eval
    # ----

    def test_eval_1 (self) :
        v = collatz_eval(1, 10)
        self.assertTrue(v == 20)

    def test_eval_2 (self) :
        v = collatz_eval(100, 200)
        self.assertTrue(v == 125)

    def test_eval_3 (self) :
        v = collatz_eval(201, 210)
        self.assertTrue(v == 89)

    def test_eval_4 (self) :
        v = collatz_eval(900, 1000)
        self.assertTrue(v == 174)
    def test_eval_5 (self) :
        v = collatz_eval(2, 47)
        self.assertTrue(v == 112)
    def test_eval_6 (self) :
        v = collatz_eval(70, 70)
        self.assertTrue(v == 15)
    def test_eval_7 (self) :
        v = collatz_eval(4, 99)
        self.assertTrue(v == 119)

    # -----
    # print
    # -----

    def test_print1 (self) :
        w = io.StringIO()
        collatz_print(w, 1, 10, 20)
        self.assertTrue(w.getvalue() == "1 10 20\n")
    def test_print2 (self) :
        w = io.StringIO()
        collatz_print(w, 100, 200, 125)
        self.assertTrue(w.getvalue() == "100 200 125\n")
    def test_print3 (self) :
        w = io.StringIO()
        collatz_print(w, 201, 210, 89)
        self.assertTrue(w.getvalue() == "201 210 89\n")
    def test_print4 (self) :
        w = io.StringIO()
        collatz_print(w, 70, 70, 15)
        self.assertTrue(w.getvalue() == "70 70 15\n")


    # -----
    # solve
    # -----

    def test_solve1 (self) :
        r = io.StringIO("1 10\n100 200\n201 210\n900 1000\n")
        w = io.StringIO()
        collatz_solve(r, w)
        self.assertTrue(w.getvalue() == "1 10 20\n100 200 125\n201 210 89\n900 1000 174\n")
    def test_solve2 (self) :
        r = io.StringIO("18 76\n7 10\n12 20\n66 65\n")
        w = io.StringIO()
        collatz_solve(r, w)
        self.assertTrue(w.getvalue() == "18 76 116\n7 10 20\n12 20 21\n66 65 28\n")
    def test_solve3 (self) :
        r = io.StringIO("16 10\n5 7\n41 6\n79 88\n")
        w = io.StringIO()
        collatz_solve(r, w)
        self.assertTrue(w.getvalue() == "16 10 18\n5 7 17\n41 6 112\n79 88 111\n")
    def test_solve4 (self) :
        r = io.StringIO("75 80\n3 32\n10 21\n69 65\n")
        w = io.StringIO()
        collatz_solve(r, w)
        self.assertTrue(w.getvalue() == "75 80 36\n3 32 112\n10 21 21\n69 65 28\n")

# ----
# main
# ----

print("TestCollatz.py")
unittest.main()
print("Done.")
