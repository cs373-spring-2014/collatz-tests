#!/usr/bin/env python

# -------------------------------
# projects/collatz/TestCollatz.py
# Copyright (C) 2014
# Glenn P. Downing
# -------------------------------

"""
To test the program:
    % python TestCollatz.py > TestCollatz.out
    % chmod ugo+x TestCollatz.py
    % TestCollatz.py > TestCollatz.out
"""

# -------
# imports
# -------

import StringIO
import unittest

from Collatz import collatz_read, collatz_eval, collatz_print, collatz_solve, collatz_one_num

# -----------
# TestCollatz
# -----------

class TestCollatz (unittest.TestCase) :
    # ----
    # read
    # ----

    def test_read_1 (self) :
        r = StringIO.StringIO("1 10\n")
        p = collatz_read(r)
        (i, j) = p.next()
        self.assert_(i ==  1)
        self.assert_(j == 10)

    def test_read_2 (self) :
        r = StringIO.StringIO("10 1\n")
        p = collatz_read(r)
        (i,j) = p.next()
        self.assert_(i == 10)
        self.assert_(j == 1)

    def test_read_3 (self) :
        r = StringIO.StringIO("\n")
        p = collatz_read(r)
        self.assert_(p.next() == [])

    def test_read_4 (self) :
        r = StringIO.StringIO(" ")
        p = collatz_read(r)
        self.assert_(p.next() == [])

    def test_read_5 (self) :
        r = StringIO.StringIO("1 2\n3 4\n")
        p = collatz_read(r)
        (i,j) = p.next()
        self.assert_(i == 1)
        self.assert_(j == 2)

    def test_read_6 (self) :
        r = StringIO.StringIO("5 5\n10 1\n")
        p = collatz_read(r)
        (i,j) = p.next()
        self.assert_(i == 5)
        self.assert_(j == 5)
        (i,j) = p.next()
        self.assert_(i == 10)
        self.assert_(j == 1)


    # ----
    # eval
    # ----

    def test_eval_1 (self) :
        v = collatz_eval((1, 10))
        self.assert_(v == 20)

    def test_eval_2 (self) :
        v = collatz_eval((100, 200))
        self.assert_(v == 125)

    def test_eval_3 (self) :
        v = collatz_eval((201, 210))
        self.assert_(v == 89)

    def test_eval_4 (self) :
        v = collatz_eval((900, 1000))
        self.assert_(v == 174)

    def test_eval_5 (self) :
        v = collatz_eval((1, 1))
        self.assert_(v == 1)

    def test_eval_6 (self) :
        v = collatz_eval((2, 1))
        self.assert_(v == 2)

    def test_eval_7 (self) :
        self.assertRaises(AssertionError, collatz_eval, (-1,1))

    def test_eval_8 (self) :
        self.assertRaises(AssertionError, collatz_eval, (0,0))

    def test_eval_8 (self) :
        self.assertRaises(AssertionError, collatz_eval, (1,1000000))



    # -----
    # print
    # -----

    def test_print_1 (self) :
        w = StringIO.StringIO()
        collatz_print(w, (1, 10), 20)
        self.assert_(w.getvalue() == "1 10 20\n")

    def test_print_2 (self) :
        w = StringIO.StringIO()
        collatz_print(w, (2, 1), 2)
        self.assert_(w.getvalue() == "2 1 2\n")

    # -----
    # solve
    # -----

    def test_solve_1 (self) :
        r = StringIO.StringIO("1 10\n100 200\n201 210\n900 1000\n")
        w = StringIO.StringIO()
        collatz_solve(r, w)
        self.assert_(w.getvalue() == "1 10 20\n100 200 125\n201 210 89\n900 1000 174\n")

    def test_solve_2 (self) :
        r = StringIO.StringIO("1 1\n2 1\n")
        w = StringIO.StringIO()
        collatz_solve(r, w)
        self.assert_(w.getvalue() == "1 1 1\n2 1 2\n")

    # -----
    # one_num
    # -----
    def test_single_num_1 (self) :
        v = collatz_one_num(1)
        self.assert_(v == 1)

    def test_single_num_2 (self) :
        v = collatz_one_num(10)
        self.assert_(v == 7)

    def test_single_num_3 (self) :
        v = collatz_one_num(999999)
        self.assert_(v == 259)

    def test_single_num_4 (self) :
        self.assertRaises(AssertionError, collatz_one_num, 0)

    def test_single_num_5 (self) :
        self.assertRaises(AssertionError, collatz_one_num, 1000000)
# ----
# main
# ----

print "TestCollatz.py"
unittest.main()
print "Done."
