#!/usr/bin/env python3

# -------------------------------
# projects/collatz/TestCollatz.py
# Copyright (C) 2014
# Glenn P. Downing
# -------------------------------

"""
To test the program:
    % python TestCollatz.py > TestCollatz.out
    % chmod ugo+x TestCollatz.py
    % TestCollatz.py > TestCollatz.out
"""

# -------
# imports
# -------

import io
import unittest

from Collatz import collatz_read_3, collatz_eval, collatz_print, collatz_solve, collatz_value

# -----------
# TestCollatz
# -----------

class TestCollatz (unittest.TestCase) :
    # ----
    # read
    # ----

    def test_read_1 (self) :
        r = io.StringIO("4 5\n")
        m = collatz_read_3(r)
        i, j = list(next(m))
        self.assertTrue(i ==  4)
        self.assertTrue(j == 5)

    def test_read_2 (self) :
        r = io.StringIO("1 10\n")
        m = collatz_read_3(r)
        i, j = list(next(m))
        self.assertTrue(i ==  1)
        self.assertTrue(j == 10)

    def test_read_3 (self) :
        r = io.StringIO("10 1\n")
        m = collatz_read_3(r)
        i, j = list(next(m))
        self.assertTrue(i ==  10)
        self.assertTrue(j == 1)

    # ----
    # eval
    # ----

    def test_eval_1 (self) :
        v = collatz_eval(1, 10)
        self.assertTrue(v == 20)

    def test_eval_2 (self) :
        v = collatz_eval(100, 200)
        self.assertTrue(v == 125)

    def test_eval_3 (self) :
        v = collatz_eval(201, 210)
        self.assertTrue(v == 89)

    def test_eval_4 (self) :
        v = collatz_eval(900, 1000)
        self.assertTrue(v == 174)

    # -----
    # print
    # -----

    def test_print (self) :
        w = io.StringIO()
        collatz_print(w, 1, 10, 20)
        self.assertTrue(w.getvalue() == "1 10 20\n")
        
    def test_print2 (self) :
        w = io.StringIO()
        collatz_print(w, 10, 1, 20)
        self.assertTrue(w.getvalue() == "10 1 20\n")

    # -----
    # solve
    # -----

    def test_solve (self) :
        r = io.StringIO("1 10\n100 200\n201 210\n900 1000\n")
        w = io.StringIO()
        collatz_solve(r, w)
        self.assertTrue(w.getvalue() == "1 10 20\n100 200 125\n201 210 89\n900 1000 174\n")

    def test_solve2 (self) :
        r = io.StringIO("1 1\n887 384\n916 778\n336 794\n")
        w = io.StringIO()
        collatz_solve(r, w)
        self.assertTrue(w.getvalue() == "1 1 1\n887 384 179\n916 778 179\n336 794 171\n")
    
    # -----
    # value
    # -----

    def test_value (self) : 
        i = collatz_value(1)
        self.assertTrue(i == 1)

    def test_value2 (self) :
        i = collatz_value(5)
        self.assertTrue(i == 6)

    def test_value3 (self) :
        i = collatz_value(327)
        self.assertTrue(i == 144)

    def test_value4 (self) : 
        i = collatz_value(1001)
        self.assertTrue(i == 143)

# ----
# main
# ----

print("TestCollatz.py")
unittest.main()
print("Done.")
