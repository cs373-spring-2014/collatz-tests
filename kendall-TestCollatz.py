#!/usr/bin/env python3

# -------------------------------
# projects/collatz/TestCollatz.py
# Copyright (C) 2014
# Glenn P. Downing
# -------------------------------

"""
To test the program:
    % python TestCollatz.py >& TestCollatz.out
    % chmod ugo+x TestCollatz.py
    % TestCollatz.py >& TestCollatz.out
"""

# -------
# imports
# -------

import io
import unittest

from Collatz import collatz_read_1, collatz_read_2, collatz_read_3, collatz_eval, collatz_print, collatz_solve

# -----------
# TestCollatz
# -----------

class TestCollatz (unittest.TestCase) :
    # ----
    # read
    # ----

    def test_read_1 (self) :
        r = io.StringIO("1 10\n")
        m = collatz_read_1(r)
        i, j = list(next(m))
        self.assertTrue(i ==  1)
        self.assertTrue(j == 10)

    def test_read_2 (self) :
        r = io.StringIO("1 10\n")
        m = collatz_read_2(r)
        i, j = list(next(m))
        self.assertTrue(i ==  1)
        self.assertTrue(j == 10)

    def test_read_3 (self) :
        r = io.StringIO("1 10\n")
        m = collatz_read_3(r)
        i, j = list(next(m))
        self.assertTrue(i ==  1)
        self.assertTrue(j == 10)

    #test our minimum and maximum inputs
    def test_read_3_extremes(self):
        r = io.StringIO("1 999999\n")
        m=collatz_read_3(r)
        i,j = list(next(m))
        self.assertTrue(i == 1)
        self.assertTrue(j == 999999)

    #the problem specification doesn't specify how much whitespace is inbetween each pair of integers
    #we should be able to handle arbitrary whitespace on an input line
    def test_read_3_whitespace(self):
        r = io.StringIO("100          200")
        m=collatz_read_3(r)
        i,j=list(next(m))
        self.assertTrue(i==100)
        self.assertTrue(j==200)

    # ----
    # eval
    # ----

    def test_eval_1 (self) :
        v = collatz_eval(1, 10)
        self.assertTrue(v == 20)

    def test_eval_2 (self) :
        v = collatz_eval(100, 200)
        self.assertTrue(v == 125)

    def test_eval_3 (self) :
        v = collatz_eval(201, 210)
        self.assertTrue(v == 89)

    def test_eval_4 (self) :
        v = collatz_eval(900, 1000)
        self.assertTrue(v == 174)

    # -----
    # print
    # -----

    def test_print (self) :
        w = io.StringIO()
        collatz_print(w, 1, 10, 20)
        self.assertTrue(w.getvalue() == "1 10 20\n")

    #make sure it works for tuples in the revrse order
    def test_print_reverse(self):
        w = io.StringIO()
        collatz_print(w, 10, 1, 20)
        self.assertTrue(w.getvalue() == "10 1 20\n")

    #test a very long string
    def test_print_extreme(self):
        w=io.StringIO()
        collatz_print(w,999999999999999,999999999999999,999999999999999)
        self.assertTrue(w.getvalue() == "999999999999999 999999999999999 999999999999999\n")

    # -----
    # solve
    # -----

    def test_solve (self) :
        r = io.StringIO("1 10\n100 200\n201 210\n900 1000\n")
        w = io.StringIO()
        collatz_solve(r, w)
        self.assertTrue(w.getvalue() == "1 10 20\n100 200 125\n201 210 89\n900 1000 174\n")

    #the answers should be correct even if the pairs are such that the first number is greater than the second
    def test_solve_reverse(self):
        r = io.StringIO("10 1\n200 100\n210 201\n1000 900\n")
        w = io.StringIO()
        collatz_solve(r,w)
        self.assertTrue(w.getvalue() == "10 1 20\n200 100 125\n210 201 89\n1000 900 174\n")

    #make sure our range is inclusive of i and j
    def test_solve_inclusiveness(self):
        r = io.StringIO("1 2\n1 1\n3 4\n")
        w = io.StringIO()
        collatz_solve(r,w)
        self.assertTrue(w.getvalue() == "1 2 2\n1 1 1\n3 4 8\n")

# ----
# main
# ----

print("TestCollatz.py")
unittest.main()
print("Done.")
