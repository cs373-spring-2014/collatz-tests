#!/usr/bin/env python3

# -------------------------------
# projects/collatz/TestCollatz.py
# Copyright (C) 2014
# Glenn P. Downing
# -------------------------------

"""
To test the program:
% python TestCollatz.py >& TestCollatz.out
% chmod ugo+x TestCollatz.py
% TestCollatz.py >& TestCollatz.out
"""

# -------
# imports
# -------

import io
import unittest

from Collatz import collatz_read, collatz_eval, collatz_print, collatz_solve, search_cache, find_cycle

# -----------
# TestCollatz
# -----------

class TestCollatz (unittest.TestCase) :
    # ----
    # read
    # ----

    def test_read (self) :
        r = io.StringIO("1 10\n")
        a = [0, 0]
        b = collatz_read(r, a)
        i, j = a
        self.assertTrue(b == True)
        self.assertTrue(i == 1)
        self.assertTrue(j == 10)

    # ------------
    # search cache
    # ------------

    def test_cache_1 (self) :
        a = [[1,1],[3,6]]
        v = search_cache(1,a)
        self.assertTrue(v == 1)

    def test_cache_2 (self) :
        a = [[0,0]]
        v = search_cache(8,a)
        self.assertTrue(v == 0)

    def test_cache_3 (self) :
        a = [[0,0]]
        v = search_cache(1000,a)
        self.assertTrue(v == 0)

    def test_cache_4 (self) :
        a = [[9,8],[56,1],[67,88],[88,7]]
        v = search_cache(88,a)
        self.assertTrue(v == 7)

    # ----------
    # find cycle
    # ----------

    def test_cycle_1 (self) :
        v = find_cycle(1)
        self.assertTrue(v == 1)

    def test_cycle_2 (self) :
        v = find_cycle(673)
        self.assertTrue(v == 65)

    def test_cycle_3 (self) :
        v = find_cycle(58)
        self.assertTrue(v == 20)

    def test_cycle_4 (self) :
        v = find_cycle(9742)
        self.assertTrue(v == 48)

    # ----
    # eval
    # ----

    def test_eval_1 (self) :
        v = collatz_eval(1, 10)
        self.assertTrue(v == 20)

    def test_eval_2 (self) :
        v = collatz_eval(100, 200)
        self.assertTrue(v == 125)

    def test_eval_3 (self) :
        v = collatz_eval(201, 210)
        self.assertTrue(v == 89)

    def test_eval_4 (self) :
        v = collatz_eval(900, 1000)
        self.assertTrue(v == 174)

    # -----
    # print
    # -----

    def test_print (self) :
        w = io.StringIO()
        collatz_print(w, 1, 10, 20)
        self.assertTrue(w.getvalue() == "1 10 20\n")

    # -----
    # solve
    # -----

    def test_solve_1 (self) :
        r = io.StringIO("1 10\n100 200\n201 210\n900 1000\n")
        w = io.StringIO()
        collatz_solve(r, w)
        self.assertTrue(w.getvalue() == "1 10 20\n100 200 125\n201 210 89\n900 1000 174\n")

    def test_solve_2 (self) :
        r = io.StringIO("6 7\n66 94\n70 35\n15 52\n")
        w = io.StringIO()
        collatz_solve(r, w)
        self.assertTrue(w.getvalue() == "6 7 17\n66 94 116\n70 35 113\n15 52 112\n")

    def test_solve_3 (self) :
        r = io.StringIO("1 1\n45 2\n27 51\n19 89\n")
        w = io.StringIO()
        collatz_solve(r, w)
        self.assertTrue(w.getvalue() == "1 1 1\n45 2 112\n27 51 112\n19 89 116\n")

    def test_solve_4 (self) :
        r = io.StringIO("62 1\n82 92\n51 50\n")
        w = io.StringIO()
        collatz_solve(r, w)
        self.assertTrue(w.getvalue() == "62 1 113\n82 92 111\n51 50 25\n")

    def test_solve_5 (self) :
        r = io.StringIO("571 520\n61 843\n")
        w = io.StringIO()
        collatz_solve(r, w)
        self.assertTrue(w.getvalue() == "571 520 137\n61 843 171\n")

# ----
# main
# ----

print("TestCollatz.py")
unittest.main()
print("Done.")
