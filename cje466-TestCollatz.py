#!/usr/bin/env python
#!/usr/bin/Python

# -------------------------------
# projects/collatz/TestCollatz.py
# Copyright (C) 2014
# Glenn P. Downing
# -------------------------------

"""
To test the program:
    % python TestCollatz.py > TestCollatz.out
    % chmod ugo+x TestCollatz.py
    % TestCollatz.py > TestCollatz.out
"""

# -------
# imports
# -------

import StringIO
import unittest

from Collatz import collatz_read, collatz_eval, collatz_print, collatz_solve, cycleLengthOf

# -----------
# TestCollatz
# -----------

class TestCollatz (unittest.TestCase) :
    # ----
    # read_1
    # ----

    def test_read_one (self) :
        r = StringIO.StringIO("1 10\n")
        a = [0, 0]
        b = collatz_read(r, a)
        self.assert_(b    == True)
        self.assert_(a[0] ==  1)
        self.assert_(a[1] == 10)

    def test_read_two (self) :
        r = StringIO.StringIO("1 999\n")
        a = [0, 0]
        b = collatz_read(r, a)
        self.assert_(b    == True)
        self.assert_(a[0] ==  1)
        self.assert_(a[1] == 999)

    def test_read_three (self) :
        r = StringIO.StringIO("999 10\n")
        a = [0, 0]
        b = collatz_read(r, a)
        self.assert_(b    == True)
        self.assert_(a[0] == 999)
        self.assert_(a[1] == 10)

    def test_read_four (self) :
        r = StringIO.StringIO("999 998\n")
        a = [0, 0]
        b = collatz_read(r, a)
        self.assert_(b    == True)
        self.assert_(a[0] == 999)
        self.assert_(a[1] == 998)

    # ----
    # eval
    # ----

    def test_eval_1 (self) :
        v = collatz_eval(1, 10)
        self.assert_(v == 20)

    def test_eval_2 (self) :
        v = collatz_eval(100, 200)
        self.assert_(v == 125)

    def test_eval_3 (self) :
        v = collatz_eval(201, 210)
        self.assert_(v == 89)

    def test_eval_4 (self) :
        v = collatz_eval(900, 1000)
        self.assert_(v == 174)

    def test_eval_5 (self) :
        v = collatz_eval(1, 100)
        w = collatz_eval(50, 100)
        self.assert_(v == w)

    def test_eval_6 (self) :
        v = collatz_eval(499, 1000)
        w = collatz_eval(750, 1000)
        self.assert_(v == w)

    def test_eval_7 (self) :
        v = collatz_eval(907275, 628500)
        self.assert_(v == 525)

    def test_eval_8 (self) :
        v = collatz_eval(9, 9)
        self.assert_(v == 20)

    # -----
    # print
    # -----

    def test_print_1 (self) :
        w = StringIO.StringIO()
        collatz_print(w, 1, 10, 20)
        self.assert_(w.getvalue() == "1 10 20\n")

    def test_print_2 (self) :
        w = StringIO.StringIO()
        collatz_print(w, 99, 200, 125)
        self.assert_(w.getvalue() == "99 200 125\n")

    def test_print_3 (self) :
        w = StringIO.StringIO()
        collatz_print(w, 900, 1000, 174)
        self.assert_(w.getvalue() == "900 1000 174\n")

    def test_print_4 (self) :
        w = StringIO.StringIO()
        collatz_print(w, 1, 23, 16)
        self.assert_(w.getvalue() == "1 23 16\n")

    # -----
    # solve
    # -----

    def test_solve_1 (self) :
        r = StringIO.StringIO("1 10\n100 200\n201 210\n900 1000\n")
        w = StringIO.StringIO()
        collatz_solve(r, w)
        self.assert_(w.getvalue() == "1 10 20\n100 200 125\n201 210 89\n900 1000 174\n")

    def test_solve_2 (self) :
        r = StringIO.StringIO("1 10\n")
        w = StringIO.StringIO()
        collatz_solve(r, w)
        self.assert_(w.getvalue() == "1 10 20\n")

    def test_solve_3 (self) :
        r = StringIO.StringIO("9 9\n")
        w = StringIO.StringIO()
        collatz_solve(r, w)
        self.assert_(w.getvalue() == "9 9 20\n")

    def test_solve_4 (self) :
        r = StringIO.StringIO("1 999\n")
        w = StringIO.StringIO()
        collatz_solve(r, w)
        self.assert_(w.getvalue() == "1 999 179\n")

    # ----------
    # cycleLengthOf
    # ----------

    def test_cycleLengthOf_1(self) :
        r  = cycleLengthOf(9)
        self.assert_(r == 20)

    def test_cycleLengthOf_2(self) :
        r  = cycleLengthOf(1)
        self.assert_(r == 1)

    def test_cycleLengthOf_3(self):
        r  = cycleLengthOf(1001)
        self.assert_(r == 143)

    def test_cycleLengthOf_4(self):
        r  = cycleLengthOf(25000)
        self.assert_(r == 127)

# ----
# main
# ----

print "TestCollatz.py"
unittest.main()
print "Done."
