#!/usr/bin/env python3

# -------------------------------
# projects/collatz/TestCollatz.py
# Copyright (C) 2014
# Glenn P. Downing
# -------------------------------

"""
To test the program:
    % python TestCollatz.py >& TestCollatz.out
    % chmod ugo+x TestCollatz.py
    % TestCollatz.py >& TestCollatz.out
"""

# -------
# imports
# -------

import io
import unittest

from Collatz import collatz_read, collatz_eval, \
     collatz_print, collatz_solve, collatz_cycle_length, \
     collatz_max_cycle_length

# -----------
# TestCollatz
# -----------

class TestCollatz (unittest.TestCase) :
    # ----
    # read
    # ----

    def test_read_normal (self) :
        r = io.StringIO("1 10\n")
        b = collatz_read(r)
        i, j = list(next(b))
        self.assertTrue(i ==  1)
        self.assertTrue(j == 10)

    def test_read_empty (self) :
        r = io.StringIO("")
        m = collatz_read(r)

        try:
            next(m)
            works = True
        except StopIteration:
            works = False
        
        self.assertTrue(works == False)

    def test_read_long_numbers (self) :
        r = io.StringIO("10000000000 1000007000")
        m = collatz_read(r)
        i, j = list(next(m))
        self.assertTrue(i ==  10000000000)
        self.assertTrue(j == 1000007000)

    # ----
    # eval
    # ----

    def test_eval_1 (self) :
        v = collatz_eval(1, 10)
        self.assertTrue(v == 20)

    def test_eval_2 (self) :
        v = collatz_eval(100, 200)
        self.assertTrue(v == 125)

    def test_eval_3 (self) :
        v = collatz_eval(201, 210)
        self.assertTrue(v == 89)

    def test_eval_4 (self) :
        v = collatz_eval(900, 1000)
        self.assertTrue(v == 174)

    # -----
    # print
    # -----

    def test_print_normal (self) :
        w = io.StringIO()
        collatz_print(w, 1, 10, 20)
        self.assertTrue(w.getvalue() == "1 10 20\n")

    def test_print_long (self) :
        w = io.StringIO()
        collatz_print(w, 132, 180892312, 10000)
        self.assertTrue(w.getvalue() == "132 180892312 10000\n")

    def test_print_zeros (self) :
        w = io.StringIO()
        collatz_print(w, 0, 0, 0)
        self.assertTrue(w.getvalue() == "0 0 0\n")


    # -----
    # solve
    # -----

    def test_solve_normal (self) :
        r = io.StringIO("1 10\n100 200\n201 210\n900 1000\n")
        w = io.StringIO()
        collatz_solve(r, w)
        self.assertTrue(w.getvalue() == "1 10 20\n100 200 125\n201 210 89\n900 1000 174\n")

    def test_solve_nothing (self) :
        r = io.StringIO("")
        w = io.StringIO()
        collatz_solve(r, w)
        self.assertTrue(w.getvalue() == "")

    def test_solve_single (self) :
        r = io.StringIO("2 2\n")
        w = io.StringIO()
        collatz_solve(r, w)
        self.assertTrue(w.getvalue() == "2 2 2\n")

    # ------------
    # cycle_length
    # ------------

    def test_cycle_length_1 (self):
        r = collatz_cycle_length(20)
        self.assertTrue(r == 8)

    def test_cycle_length_2 (self):
        r = collatz_cycle_length(99)
        self.assertTrue(r == 26)

    def test_cycle_length_3 (self):
        r = collatz_cycle_length(10)
        self.assertTrue(r == 7)

    # ----------------
    # max_cycle_length
    # ----------------

    def test_max_cycle_length_1 (self):
        r = collatz_max_cycle_length(200, 12)
        self.assertTrue(r == 125)

    def test_max_cycle_length_2 (self):
        r = collatz_max_cycle_length(1, 1)
        self.assertTrue(r == 1)

    def test_max_cycle_length_3 (self):
        r = collatz_max_cycle_length(9, 10)
        self.assertTrue(r == 20)

# ----
# main
# ----

print("TestCollatz.py")
unittest.main()
print("Done.")
